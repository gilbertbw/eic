
/*
 * GET users listing.
 */

exports.list = function(req, res){
  res.send("respond with a resource");
};

exports.login = function(req, res) {
    // TODO: handle login attempts
    var userUsername = req.body.username
      , userPassword = req.body.password
      , userSchool = req.body.school
      , mongo = require('globals').mongo
      , passwordHash = require('password-hash');
    
    // ***find school with username client supplied***
    mongo.schools.findOne({username:userSchool},{'_id':1}, function(err,school){
        if(school===null){
            req.flash('error','That school does not exist');
            res.redirect('/login');
        }
        // find user with username client supplied & school with id of school with username client specified
        mongo.users.findOne({username:userUsername,school:school._id}, function(err, user) {
        if(err){
            console.log("DB error - " + err);
            // TODO handle DB errors zlib.createDeflateRaw(o);
        }else if(user === null){
            req.flash('error','That user does not exist within the school '+userSchool);
            res.redirect('/login/');
            console.log(userUsername + " doesnt exist");
        }else{
            if(passwordHash.verify(userPassword,user.password)){
                console.log("Correct password!");
                req.session.id = user._id;
                // req.session.id = "yo man!";
                res.redirect('/restricted');
                // res.redirect('/');
            }else{
                req.flash('error','Incorrect password');
            }
        }
    });
    });
    console.log(userUsername+" from the school of "+userSchool+" just attempted to login!");
    // res.redirect('/login');
};

exports.add = function(req, res) {
    var mongo = require('globals').mongo,
        ObjectID = require('globals').ObjectID,
        passwordHash = require('password-hash');
        var client = {};
        console.log(req.body.school);
        mongo.schools.findOne({username:req.body.school},{_id:1},function(err, school) {
        if(err === null){
            if(school){
                client.firstname = req.body.firstName;
                client.lastname = req.body.lastName;
                client.username = req.body.username;
                client.level = req.body.level;
                client.email = req.body.email;
                client.password = passwordHash.generate(req.body.password);
                client.school = school._id;
                console.log(client);
                mongo.users.insert(client,function(err,result) {
                    if(err === null){
                        req.flash('info',"User added successfully -- "+client.firstname);
                        res.redirect('users/add');
                    }else{
                        req.flash('info',"Error "+err+" occured");
                        res.redirect('users/add');
                    }
                });
            }else{
                console.log('/users/add -- no school');
                req.flash('info',"No school entered");
                res.redirect('users/add');
            }
        }else{
            console.log(err);
        }
    });
};
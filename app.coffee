express = require('express')
fs = require('fs')
routes = require('./routes')
partials = './views/partials'
user = require('./routes/user')
http = require('http')
hbs = require('hbs')
globals = require('./globals')
flash = require('connect-flash')
passport = require('passport')
passportlocal = require('passport-local')
path = require('path')
auth = require('auth')
# school = require('./routes/school')
tasks = require('./routes/tasks')
student = require('./routes/student')
teacher = require('./routes/teacher')
# schooladmin = require('./routes/schooladmin')
# admin = require('./routes/admin')

app = express()

# assign the handlebars engine to html
app.engine('html', require('hbs').__express)

# register all partials in /views/partials
partialsDir = __dirname + '/views/partials'

filenames = fs.readdirSync(partialsDir)


# Configure the server
app.configure ->
  app.set('port', process.env.PORT || 3000)
  app.set('views', __dirname + '/views')
  app.set('view engine', 'html')
  app.use(express.cookieParser('keyboard cat shall keep his hands out dat cookie jar'))
  app.use(express.cookieSession({cookie: { path: '/', httpOnly: true, maxAge: null},secret:'cookies are safe!', key: 'sid'}))
  app.use(flash())
  app.use(express.favicon())
  app.use(express.logger('dev'))
  app.use(express.bodyParser())
  app.use(expreass.methodOverride())
  app.use(app.router)
  app.use(express.static(path.join(__dirname, 'public')))

app.configure 'development', ->
  app.use express.errorhandler()

app.get '/', routes.index
app.get '/login',routes.login
app.get '/users/add',routes.usersadd

app.get '/student',auth.verify(1),student.home
# app.get '/teacher',auth.verify(2),teacher.home
# app.get '/teacher/task/add',auth.verify(2),teacher.task/add
# app.get '/schooladmin',auth.verify(3),schooladmin.index
# app.get '/admin',auth.verify(4),admin.index

app.get '/teacher/task/add',auth.verify(2), teacher.taskadd
app.post '/tasks/add', auth.verify(2), tasks.add

app.post '/users/login', user.login
app.post '/users/add', user.add
module.exports = {
    mongo: require('mongo-lazy').open({
        db: 'test',
        host: 'localhost',
        port: '27017'
        // , user:
        // , password:
    }),
    ObjectID: require('mongo-lazy').ObjectID
    // redis: require('redis').createClient()
};
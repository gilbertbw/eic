export var user = new Schema({
    username: String,
    password: String,
    level: Number,
    name: {
        first: String,
        last: String
    },
    tasks:[{id: ObjectId}],
    email: String,
    school: ObjectId,
    homework: [{
        id: ObjectId,
        done: Boolean,
        score: [{
            page: number,
            score: number,
            max: number
        }]
    }]
});

export var homework = new Schema({
    homework: ObjectId,
    setter: ObjectId,
    due: Date
});

export var homeworkTemplate = new Schema({
    name: String,
    pages: [{
        num: Number,
        questions: [{
            // this may not need to be schemaless?
        }]
    }],
    subject: String,
    year: Number,
    type: String
});

export var lessons = new Schema({
    name: String,
    subject: String,
    level: Number,
    pages: [{
        num: String,
        content: [{type: String,content: {}}]
    }]
});

export var school = new Schema({
    due: Date,
    state: Number,
    phone: Number,
    email: String,
    payment: {}     
});

export var class = new Schema({
    name: String,
    school: ObjectId,
    year: Number, 
});